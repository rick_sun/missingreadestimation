import pandas as pd
import numpy as np
import pickle
import os
import sklearn
from sklearn.ensemble import RandomForestRegressor
from functions import engineer_features
from sklearn.externals.joblib import dump,load
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('--train_data', type=str, dest='train_data', help='training data')
args = parser.parse_args()

train_data = args.train_data
df_raw = pd.read_csv(train_data,delimiter=',',dtype={'SA_ID':str})
df = engineer_features(df_raw)

train_column_list = df.drop('CURRENT_USAGE',axis=1).columns.values.tolist()

with open('outputs/train_column_list.csv', 'w') as f:
    for item in train_column_list:
        f.write("%s," % item)      
f.close()    

with open('outputs/train_column_list.csv', 'rb+') as f:
    f.seek(-1, os.SEEK_END)
    f.truncate()
f.close()

output_y = np.asarray(df.CURRENT_USAGE)
output_y = np.reshape(output_y,(len(output_y),1))

input_x = df.drop(['CURRENT_USAGE'],axis=1)
input_x = np.asarray(input_x)

model_rfg = RandomForestRegressor(n_estimators=20)

model_rfg.fit(input_x,output_y)

dump(model_rfg,'outputs/model_estimation.mdl')
