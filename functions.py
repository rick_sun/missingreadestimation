import numpy as np
import pandas as pd
def engineer_features(df):
    df = df.set_index('SA_ID')
    df = df.drop_duplicates()
    df['CURRENT_DURATION'] = (pd.to_datetime(df.CURRENT_END_DATE) - pd.to_datetime(df['CURRENT_START_DATE'])).dt.days
    df['PREVIOUS_DURATION'] = (pd.to_datetime(df.PREVIOUS_END_DATE) - pd.to_datetime(df['PREVIOUS_START_DATE'])).dt.days
    df['SIX_MONTHS_DURATION'] = (pd.to_datetime(df.SIX_MONTHS_END_DATE) - pd.to_datetime(df['SIX_MONTHS_START_DATE'])).dt.days
    df['NINE_MONTHS_DURATION'] = (pd.to_datetime(df.NINE_MONTHS_END_DATE) - pd.to_datetime(df['NINE_MONTHS_START_DATE'])).dt.days
    df['LAST_YEAR_DURATION'] = (pd.to_datetime(df.LAST_YEAR_END_DATE) - pd.to_datetime(df['LAST_YEAR_START_DATE'])).dt.days

    df = df.drop(['CURRENT_START_DATE','CURRENT_END_DATE','PREVIOUS_START_DATE','SIX_MONTHS_START_DATE','SIX_MONTHS_END_DATE',
                  'NINE_MONTHS_START_DATE','NINE_MONTHS_END_DATE','PREVIOUS_END_DATE','LAST_YEAR_START_DATE','LAST_YEAR_END_DATE'],axis=1)

    df = df.rename(columns = {'HOUSE_TYPE':'ROAD_TYPE','ADDRESS2':'ROAD_NAME','LAND SIZE':'LAND_SIZE'})

    df['PROPERTY_TYPE'] = pd.np.where(df.COUNTY.str.contains("UNIT"), "UNIT",
                       pd.np.where(df.COUNTY.str.contains("FLAT"), "APT",
                       pd.np.where(df.COUNTY.str.contains("APT"), "APT",
                       pd.np.where(df.COUNTY.str.endswith("A"), "UNIT",
                       pd.np.where(df.COUNTY.str.contains("B"), "UNIT",
                       pd.np.where(df.COUNTY.str.contains("C"), "UNIT","HOUSE"))))))

    df.PREVIOUS_USAGE = df.PREVIOUS_USAGE.fillna(df.PREVIOUS_USAGE.median())
    df.LAST_YEAR_USAGE = df.LAST_YEAR_USAGE.fillna(df.LAST_YEAR_USAGE.median())

    df.METER_GROUP = df.METER_GROUP.fillna('E')

    df.LAST_YEAR_DURATION = df.LAST_YEAR_DURATION.fillna(df.LAST_YEAR_DURATION.median())
    df.NINE_MONTHS_DURATION = df.NINE_MONTHS_DURATION.fillna(df.NINE_MONTHS_DURATION.median())
    df.SIX_MONTHS_DURATION = df.SIX_MONTHS_DURATION.fillna(df.SIX_MONTHS_DURATION.median())
    df.PREVIOUS_DURATION = df.PREVIOUS_DURATION.fillna(df.PREVIOUS_DURATION.median())
    df.SIX_MONTHS_USAGE = df.SIX_MONTHS_USAGE.fillna(df.SIX_MONTHS_USAGE.median())
    df.NINE_MONTHS_USAGE = df.NINE_MONTHS_USAGE.fillna(df.NINE_MONTHS_USAGE.median())

    df.LAST_YEAR_ESTIMATED = df.LAST_YEAR_ESTIMATED.fillna('UNKNOWN')
    df.NINE_MONTHS_ESTIMATED = df.LAST_YEAR_ESTIMATED.fillna('UNKNOWN')
    df.SIX_MONTHS_ESTIMATED = df.LAST_YEAR_ESTIMATED.fillna('UNKNOWN')
    df.PREVIOUS_ESTIMATED = df.PREVIOUS_ESTIMATED.fillna('UNKNOWN')
    df['PREVIOUS_ESTIMATED'] = df.PREVIOUS_ESTIMATED.apply(lambda x: 'N' if x=='N' else 'Y')
    df['SIX_MONTHS_ESTIMATED'] = df.PREVIOUS_ESTIMATED.apply(lambda x: 'N' if x=='N' else 'Y')
    df['NINE_MONTHS_ESTIMATED'] = df.PREVIOUS_ESTIMATED.apply(lambda x: 'N' if x=='N' else 'Y')
    df['LAST_YEAR_ESTIMATED'] = df.PREVIOUS_ESTIMATED.apply(lambda x: 'N' if x=='N' else 'Y')

    df['MR_INSTR_CD'] = df.MR_INSTR_CD.apply(lambda x: 0 if x=='00  ' else 1)
    df.ACCT_REL_TYPE_CD = df.ACCT_REL_TYPE_CD.fillna('UNKNOWN')
    df['ACCT_REL_TYPE_CD'] =  df.ACCT_REL_TYPE_CD.apply(lambda x: "TENANT" if x not in ['OWNER   ','UNKNOWN']  else x)
    df = df.drop (['TREND_USAGE','COUNTY'],axis=1)
    df = onehot_encode(df,['OK_TO_ENTER_SW', 'MR_INSTR_CD','ROAD_NAME','CITY','ROAD_TYPE','IN_CITY_LIMIT','PROPERTY_TYPE','ACCT_REL_TYPE_CD','METER_GROUP',
                          'LAST_YEAR_ESTIMATED','NINE_MONTHS_ESTIMATED','SIX_MONTHS_ESTIMATED','PREVIOUS_ESTIMATED'])                                                       
    return df


def onehot_encode(d, cols):
    for c in cols:
        d = pd.concat([d,pd.get_dummies(d[c], prefix='key_'+c)],axis=1) 
    return d.drop(cols,axis=1)   
