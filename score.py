from azureml.core.model import Model
from sklearn.externals import joblib
from functions import engineer_features
import pandas as pd
import numpy as np
import argparse
import json

def init():
    global model
    model_path = Model.get_model_path('est')
    model = joblib.load(model_path)
    
def run(data):
    try:
        df_train = pd.read_csv('train_data.csv')
        
        df_raw = pd.read_json(data,orient ='records',dtype={'SA_ID':str})        
        df_raw['CURRENT_USAGE']=0
        
        df_all = pd.concat([df_train,df_raw],sort=False)
        df_all = engineer_features(df_all)
        
        df_score = df_all[-df_raw.shape[0]:][df_all.columns[1:]]
        
        df_train_cols = pd.read_csv('train_column_list.csv')
        missing_cols = set( df_train_cols.columns ) - set( df_score.columns )
        abundant_cols = set( df_score.columns )- set( df_train_cols.columns )

        for c in missing_cols:
            df_score[c] = 0
        for c in abundant_cols:
            df_score = df_score.drop(c,axis=1)
            
        df_score['RESULT'] = model.predict(np.asarray(df_score))
        
        df_score = df_score.reset_index(drop=False)
        result = df_score[['SA_ID','RESULT']]
        return result.to_json(orient ='records')
    except Exception as e:
        return str(e)
